import PlaylistDetails from "./PlaylistDetails"
import { shallow, ShallowWrapper } from "enzyme"
import { Playlist } from "../model/Playlist"
import React from "react"



describe('PlaylistDetails', () => {
  const setUp = () => {
    let wrapper: ShallowWrapper
    const playlistMock: Playlist = Object.freeze({
      id: '123',
      type: 'playlist',
      name: 'Testowa',
      description: 'some description',
      public: true,
    })
    let onEditMock: jest.Mock
  
    onEditMock = jest.fn()
  
    wrapper = shallow(<PlaylistDetails
      playlist={playlistMock} onEdit={onEditMock} />)
  
    return {
      wrapper, playlistMock, onEditMock,
      // Page Object Pattern
      getName: () => wrapper.find('[data-test="playlist_name"]'),
      getPublic: () => wrapper.find('[data-test="playlist_public"]'),
      getDescription: () => wrapper.find('[data-test="playlist_description"]'),
      getButton: () => wrapper.find('[data-test="edit-button"]')
    }
  }

  it('should render', () => {
    const { wrapper } = setUp()
    expect(wrapper).toExist()
  })

  it('shows playlist details', () => {
    const { getName, getDescription, getPublic } = setUp()
    expect(getName()).toHaveText('Testowa')
    expect(getPublic()).toHaveText('Yes')
    expect(getDescription()).toHaveText('some description')
  })

  it('updates when playlist changes', () => {
    const { getName, getDescription, getPublic, wrapper, playlistMock } = setUp()

    wrapper.setProps({
      playlist: {
        ...playlistMock,
        name: 'NowyName', public: false, description: 'InnyOpis'
      }
    })
    expect(getName()).toHaveText('NowyName')
    expect(getPublic()).toHaveText('No')
    expect(getDescription()).toHaveText('InnyOpis')
  })

  it('notifies parent when Edit is clicked', () => {
    const { onEditMock, getButton } = setUp()

    const button = getButton()

    button.simulate('click')

    expect(onEditMock).toHaveBeenCalled()
  })
})
