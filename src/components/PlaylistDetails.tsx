// tsrafce
import React, { FC } from 'react'
import { Playlist } from '../model/Playlist'

interface Props {
  playlist: Playlist
  onEdit: () => void
}

const PlaylistDetails: FC<Props> = React.memo(({ playlist, onEdit }) => {

  return (
    <div>
      <dl>
        <dt>Name:</dt>
        <dd data-test="playlist_name">{playlist?.name}</dd>

        <dt>Public:</dt>
        <dd data-test="playlist_public">{playlist?.public ? 'Yes' : 'No'}</dd>

        <dt>Description:</dt>
        <dd data-test="playlist_description">{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={onEdit} data-test="edit-button">
        Edit
      </button>
    </div>
  )
})


export default PlaylistDetails
