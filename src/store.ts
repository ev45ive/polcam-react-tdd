import { PlaylistsState } from "./reducers/playlists";


export interface AppState{
  playlists: PlaylistsState
}