import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { AppState } from '../store'
import PlaylistList from '../components/PlaylistList'
import { Playlist } from '../model/Playlist'
import { actions } from '../reducers/playlists'

interface Props {

}

const ReduxPlaylists = (props: Props) => {
  const playlists = useSelector<AppState, Playlist[]>(state => state.playlists.playlists)
  const dispatch = useDispatch()

  const onSelected = ({ id }: Playlist) => {
    
    dispatch(actions.select(id))
  }
  const selected = undefined

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList playlists={playlists} onSelected={onSelected} selected={selected} />
        </div>
        <div className="col">

        </div>
      </div>

    </div>
  )
}

export default ReduxPlaylists
