import reducer, { PLAYLISTS_LOAD_SUCCESS, actions } from "./playlists";
import { AnyAction } from "redux";
import { Playlist } from "../model/Playlist";

describe('Playlists Reducer', () => {

  it('has initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual({
      playlists: [],
      selected: null
    })
  });

  it('creates load action', () => {
    const mockData = [1, 2, 3] as unknown as Playlist[]

    expect(actions.loadSuccess(mockData)).toEqual<PLAYLISTS_LOAD_SUCCESS>({
      type: 'PLAYLISTS_LOAD_SUCCESS',
      payload: {
        data: mockData
      }
    })
  })
  
  it('loads playlists', () => {
    const mockData = [1, 2, 3] as unknown as Playlist[]
    expect(reducer(undefined, actions.loadSuccess(mockData))).toEqual({
      playlists: mockData,
      selected: null
    })
  })

  it('selects playlist', () => {
    const mockData = [{ id: '123' }, { id: '234' }] as unknown as Playlist[]

    expect(reducer({
      playlists: mockData,
      selected: null
    }, actions.select('123'))).toEqual({
      playlists: mockData,
      selected: '123'
    })
  })

  it('update playlist', () => {
    const stateBefore = [{ id: '123' }, { id: '234', name: 'before' }]

    const stateAfter = [{ id: '123' }, { id: '234', name: 'after' }]

    const playlist = { id: '234', name: 'after' };

    expect(reducer({
      playlists: stateBefore as unknown as Playlist[],
      selected: null
    }, actions.update(playlist as unknown as Playlist)))
      .toHaveProperty('playlists', stateAfter)
  })

});