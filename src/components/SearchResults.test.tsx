import { shallow, mount } from "enzyme";
import SearchResults from "./SearchResults";
import React from "react";
import { Album } from "../model/Album";
import albumData from "../model/__mocks__/album-data";
import AlbumCard from "./AlbumCard";

describe("SearchResults", () => {
  const setUp = () => {
    // const wrapper = shallow()
    const mockResults: Album[] = [];
    const wrapper = mount(<SearchResults results={mockResults} />);
    const getResultItems = () => wrapper.find(AlbumCard);
    return { wrapper, mockResults, getResultItems };
  };

  it("renders", () => {
    const { wrapper } = setUp();
    expect(wrapper).toExist();
  });

  it("renders empty list", () => {
    const { wrapper } = setUp();
    // expect(getResultItems()).not.toExist()
    expect(wrapper).toMatchInlineSnapshot(`
      <SearchResults
        results={Array []}
      >
        <div
          className="card-group"
        />
      </SearchResults>
    `);
  });

  it("renders results", () => {
    const { wrapper } = setUp();
    wrapper.setProps({ results: albumData });

    expect(wrapper).toMatchSnapshot("Example album search results");
  });
});
