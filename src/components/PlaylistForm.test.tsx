import React from "react";
import { shallow, ShallowWrapper } from "enzyme"
import PlaylistForm from "./PlaylistForm"
import { Playlist } from "../model/Playlist";


jest.mock('react', () => ({
  ...jest.requireActual('react'),
  createRef: jest.fn().mockReturnValue({
    current: {
      focus: jest.fn()
    }
  })
}), {})

describe('PlaylistForm', () => {

  const setUp = () => {
    const playlist: Playlist = {
      id: '123',
      type: 'playlist',
      name: 'Testowa',
      description: 'some description',
      public: true,
    },
      onCancel = jest.fn(),
      onSave = jest.fn();

    type Props = {
      playlist: Playlist;
    };

    type State = {
      playlist: Playlist;
    };

    const wrapper: ShallowWrapper<Props, State, PlaylistForm> = shallow(
      <PlaylistForm
        playlist={playlist}
        onCancel={onCancel}
        onSave={onSave} />)

    const getNameInput = () => wrapper.find('[name="name"]')
    const getPublicInput = () => wrapper.find('[name="public"]')
    const getDescriptionInput = () => wrapper.find('[name="description"]')
    const getCancelButton = () => wrapper.find('[data-test="cancel-button"]')
    const getSaveButton = () => wrapper.find('[data-test="save-button"]')

    return {
      wrapper, playlist, onCancel, onSave,
      getNameInput,
      getPublicInput,
      getDescriptionInput,
      getCancelButton,
      getSaveButton
    }
  }


  it('should render', () => {
    const { wrapper } = setUp()
    expect(wrapper).toExist()
  })

  it('should focus name field', () => {
    const { wrapper } = setUp()

    expect(wrapper.instance().nameInputRef.current!.focus).toHaveBeenCalled()
  })

  it('shows playlist data', () => {
    const { wrapper, getNameInput, getPublicInput, getDescriptionInput, playlist } = setUp()

    expect(getNameInput()).toHaveValue(playlist.name)
    expect(getPublicInput()).toBeChecked()
    // expect(getPublicInput()).not.toBeChecked()
    expect(getDescriptionInput()).toHaveValue(playlist.description)
  })

  it('notifies parent when cancel is clicked', () => {
    const { getCancelButton, onCancel } = setUp()

    getCancelButton().simulate('click')

    expect(onCancel).toHaveBeenCalled()
  })


  it('notifies parent when save is clicked', () => {
    const { getSaveButton, onSave, playlist } = setUp()

    getSaveButton().simulate('click')

    expect(onSave).toHaveBeenCalledWith(playlist)
  })

  it('updates playlist when input changes', () => {
    const { wrapper, getNameInput } = setUp()

    getNameInput().simulate('change', {
      target: { value: 'Placki', name: 'name', type: 'text' }
    })
    expect(wrapper.state('playlist')).toHaveProperty('name', 'Placki')

  })

})                 
