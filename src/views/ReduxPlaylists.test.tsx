import { shallow, mount } from "enzyme"
import React from "react"
import ReduxPlaylists from "./ReduxPlaylists"
import PlaylistList from "../components/PlaylistList"
import PlaylistDetails from "../components/PlaylistDetails"
import PlaylistForm from "../components/PlaylistForm"
import { Provider } from "react-redux"
import { createStore, Reducer } from "redux"


jest.mock('../components/PlaylistList', () => ({
  __esModule: true, default: () => "PlaylistList"
}))
jest.mock('../components/PlaylistDetails', () => ({
  __esModule: true, default: () => "PlaylistDetails"
}))
jest.mock('../components/PlaylistForm', () => ({
  __esModule: true, default: () => "PlaylistForm"
}))

describe('ReduxPlaylists', () => {

  // https://redux.js.org/recipes/writing-tests#connected-components
  const setUp = (initialState: any) => {
    const reducer = jest.fn(state => state)
    const store = createStore(reducer, initialState)
    const wrapper = mount(<Provider store={store}>
      <ReduxPlaylists />
    </Provider>)

    const getList = () => wrapper.find(PlaylistList)
    const getDetails = () => wrapper.find(PlaylistDetails)
    const getForm = () => wrapper.find(PlaylistForm)

    return {
      wrapper, getList,
      getDetails,
      getForm,
      reducer
    }
  }

  it('renders', () => {
    const { wrapper } = setUp({
      playlists: {
        playlists: []
      }
    })
    expect(wrapper).toExist()
  })

  it('shows playlists list', () => {
    const playlists = [1, 2, 3] as any

    const { wrapper, getList } = setUp({
      playlists: {
        playlists
      }
    })

    expect(getList()).toExist()
    expect(getList().prop('playlists')).toBe(playlists)
  })

  it('selects playlist', () => {
    const playlists = [1, 2, 3] as any

    const { wrapper, getList, reducer } = setUp({
      playlists: {
        playlists
      }
    })

    // Skip redux @INIT action:
    reducer.mockClear()

    // Fake user selecting playlist
    getList().invoke('onSelected')({ id: 123 } as any)

    // expect(reducer).toHaveBeenCalledWith(state,action)
    const [stateAfter, action] = reducer.mock.calls[0] as any
    expect(action).toEqual({ "payload": { "id": 123 }, "type": "PLAYLISTS_SELECT" })
  })

  it('shows selected playlist form', () => { })
})