import React from 'react';
import { shallow, mount } from 'enzyme';
// import { render } from '@testing-library/react';
import App from './App';
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'

import Playlists from './views/Playlists';
import MusicSearch from './views/MusicSearch';

jest.mock('./views/Playlists', () => ({
  __esModule: true, // this property makes it work
  default: () => "Playlists"
}))
jest.mock('./views/MusicSearch', () => ({
  __esModule: true, // this property makes it work
  default: () => "MusicSearch"
}))

// https://testing-library.com/docs/example-react-router
// https://reactrouter.com/web/guides/testing
describe('App component', () => {
  const setUp = () => {
    const history = createMemoryHistory()
    history.push('/')

    const wrapper = mount(
      <Router history={history}>
        <App />
      </Router>
    )

    return { wrapper, history }
  }

  it('renders Hello React ', () => {
    const { wrapper, history } = setUp();

    expect(wrapper.find('a.navbar-brand')).toExist()
    // expect(wrapper.find('a').text()).toMatch('Hello')
    expect(wrapper.find('a.navbar-brand')).toHaveText('Music App')
  });
  

  it('shows default view', () => {
    const { wrapper, history } = setUp();
    history.push('/')
    wrapper.update()
    expect(history.location.pathname).toEqual('/playlists')
    expect(wrapper.find(Playlists)).toExist()
  })


  it('shows playlists view', () => {
    const { wrapper, history } = setUp();
    history.push('/playlists')
    wrapper.update()
    expect(wrapper.find(Playlists)).toExist()
  })

  it('shows music search view', () => {
    const { wrapper, history } = setUp();
    history.push('/search')
    wrapper.update()
    expect(wrapper.find(MusicSearch)).toExist()
  })

  it('has top navigation', () => {
    const { wrapper, history } = setUp();
    
    wrapper.find({ to: '/search' }).simulate('click', { button: 0 })
    expect(history.location.pathname).toEqual('/search')

    wrapper.find({ to: '/playlists' }).simulate('click', { button: 0 })
    expect(history.location.pathname).toEqual('/playlists')
  })

  // FIXME: Move to MusicSearch
  // it('search with query', () => {
  //   const { wrapper, history } = setUp();

  //   history.push({ pathname: '/search', search: '?q=placki' })
  //   wrapper.update()
    
  //   expect(wrapper.find(MusicSearch).prop('location')).toEqual(123)

  // })


})


