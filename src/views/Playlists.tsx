// tsrcc
import React, { Component } from 'react'
import PlaylistDetails from '../components/PlaylistDetails'
import { Playlist } from '../model/Playlist'
import PlaylistForm from '../components/PlaylistForm'
import PlaylistList from '../components/PlaylistList'

interface Props {

}

interface State {
  selected?: Playlist,
  playlists: Playlist[],
  mode: 'show' | 'edit'
}

export default class Playlists extends Component<Props, State> {
  state: State = {
    selected: {
      id: '123',
      type: 'playlist',
      name: 'Testowa',
      description: '',
      public: true,
    },
    playlists: [
      {
        id: '123',
        type: 'playlist',
        name: 'Testowa 123',
        description: '',
        public: true,
      }, {
        id: '234',
        type: 'playlist',
        name: 'Testowa 234',
        description: '',
        public: false,
      }, {
        id: '345',
        type: 'playlist',
        name: 'Testowa 345',
        description: '',
        public: true,
      },
    ],
    mode: 'show'
  }

  // componentDidMount(){
  //   fetch('http://google.com/')
  // }

  select = (selected: Playlist) => {
    this.setState({
      selected
    })
  }

  edit = () => {
    this.setState({mode:'edit'})
  }
  cancel = () => {
    this.setState({mode:'show'})
  }
  save(){}

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <input type="text" className="form-control mb-3" onChange={()=>this.setState({})}/>
            <PlaylistList
              playlists={this.state.playlists}
              selected={this.state.selected}
              onSelected={this.select} />
          </div>
          <div className="col">
            {
              this.state.selected ? <>

                {this.state.mode === 'show' && <PlaylistDetails playlist={this.state.selected} onEdit={this.edit} />}
                {this.state.mode === 'edit' && <PlaylistForm playlist={this.state.selected} onCancel={this.cancel} onSave={this.save} />}

              </> : <p>Please select playlist</p>
            }
          </div>
        </div>

      </div>
    )
  }
}
