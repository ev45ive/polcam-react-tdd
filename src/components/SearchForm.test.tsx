import React from 'react'
import { mount } from 'enzyme';
import SearchForm from './SearchForm';

describe('SearchForm', () => {

  const setUp = () => {
    // const wrapper = shallow()
    const onSearch = jest.fn()

    const wrapper = mount(<SearchForm onSearch={onSearch} query="" />);

    const getQueryInput = () => wrapper.find('[name="query"]')
    const getSearchButton = () => wrapper.find('[data-test="search-button"]')

    return { wrapper, onSearch, getQueryInput, getSearchButton };
  };

  it('renders', () => {
    const { wrapper } = setUp()
    expect(wrapper).toExist()
  })

  it('doesnt emit on mount', () => {
    jest.useFakeTimers();
    const { wrapper, onSearch } = setUp()
    jest.runAllTimers()
    expect(onSearch).not.toHaveBeenCalled()
  })

  it('emits search query to parent', () => {
    const { wrapper, getQueryInput, getSearchButton, onSearch } = setUp()

    getQueryInput().simulate('change', {
      target: { value: 'Placki' }
    })

    getSearchButton().simulate('click')

    expect(onSearch).toHaveBeenCalledWith('Placki')
  })

  it('searches when typing text with debounce 300ms', () => {

    jest.useFakeTimers();

    const { wrapper, getQueryInput, onSearch } = setUp()

    getQueryInput().simulate('change', {
      target: { value: 'Placki' }
    })

    // after 299ms
    jest.advanceTimersByTime(299)
    expect(onSearch).not.toHaveBeenCalled()

    getQueryInput().simulate('change', {
      target: { value: 'Placki2' }
    })
    expect(onSearch).not.toHaveBeenCalled()

    // after 301ms
    jest.advanceTimersByTime(301)
    expect(onSearch).toHaveBeenNthCalledWith(1, 'Placki2')

  })

  it('shows query from parent', () => {
    const { wrapper, getQueryInput, onSearch } = setUp()

    wrapper.setProps({ query: 'FromParent' })
    // Next render after useEffect
    wrapper.update()

    expect(getQueryInput().prop('value')).toEqual('FromParent')
  })

  afterEach(() => {
    jest.useRealTimers()
    jest.clearAllTimers()
  })

})