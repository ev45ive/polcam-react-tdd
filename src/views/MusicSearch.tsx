import React, { useState } from 'react';
import { searchAlbums } from '../services/search'
import { Album } from '../model/Album';
import SearchForm from '../components/SearchForm';
import SearchResults from '../components/SearchResults';

const MusicSearch = () => {
  const [results, setResults] = useState<Album[]>([])
  const [message, setmessage] = useState('')

  const search = (query: string) => {
    searchAlbums(query).then(resp => {
      setResults(resp)
    }).catch(err=> {
      setmessage(err.message)
    })
  }


  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} query={''} />

        </div>
      </div>
      <div className="row">
        <div className="col">
          {message}
          <SearchResults results={results} />

        </div>
      </div>
    </div>
  );
};
export default MusicSearch;
