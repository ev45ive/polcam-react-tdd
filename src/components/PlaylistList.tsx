
// tsrafce
import React, { FC } from 'react'
import { Playlist } from '../model/Playlist'

interface Props {
  // playlist: Array<Playlist>
  // onSelected: (selected: Playlist) => void

  selected?: Playlist
  playlists: Playlist[]
  onSelected(selected: Playlist): void
}

const PlaylistList: FC<Props> = ({ playlists, onSelected, selected }) => {

  return (
    <div>
      <div className="list-group">
        {playlists.map((playlist, index) =>
          <div className={`list-group-item ${
            playlist.id === selected?.id ? 'active' : ''
            }`} key={playlist.id} onClick={() => onSelected(playlist)}>
            {index + 1}. {playlist.name}
          </div>
        )}
      </div>
    </div>
  )
}

export default PlaylistList
