import { authorize, extractToken, getToken } from "./auth";
import authConfig from "./auth.config";
jest.mock('./auth.config')

describe.only('Auth service', () => {
  const { location } = window

  beforeAll(() => {
    //FIX:Property XX does not have access type get
    delete window.location;
    window.location = Object.defineProperties({}, {
      hash: { configurable: true, get: jest.fn() },
      href: { configurable: true, get: jest.fn(), set: jest.fn() },
      replace: { configurable: true, get: jest.fn() },
      reload: { configurable: true, get: jest.fn() },
    }) as any
  })
  afterAll(() => {
    window.location = location
  })

  it('Logs user in', () => {

    const redirectSpy = jest.spyOn(window.location, 'href', 'set');
    authorize()

    expect(redirectSpy).toHaveBeenCalledWith(
      `AuthorizeURL?client_id=CLIENT_ID&redirect_uri=APP_URL&response_type=token`
    )
  });

  it('Extracts token after successful login', () => {

    const hashSpy = jest.spyOn(window.location, 'hash', 'get');
    hashSpy.mockReturnValue('#access_token=MOCK_TOKEN')
    extractToken()
    expect(getToken()).toEqual('MOCK_TOKEN')
  })

  // it('auto logs in when no token', () => {

  // })

});