import { Album } from "../../model/Album";

export const searchAlbums = jest.fn().mockResolvedValue([
  { id: '1', name: '1' },
  { id: '2', name: '2' },
  { id: '3', name: '3' },
] as Album[])