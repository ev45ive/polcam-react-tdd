import * as search from "./search";
import { AlbumSearchResponse, Album } from "../model/Album";
import { getToken } from "./auth"

jest.mock('./auth', () => ({
  getToken: jest.fn().mockReturnValue('FAKE_TOKEN')
}));

// import axios from 'axios'
// jest.mock('axios');

describe('Search Service', () => {

  const setUp = () => {
    const fetchSpy = jest.spyOn(window, 'fetch')
    // const fakeGetToken = jest.spyOn(search, 'getToken')

    return {
      fetchSpy, fakeGetToken: getToken as jest.Mock
    }
  }

  it('fetch search results from server', () => {
    const { fetchSpy } = setUp()

    const mockAlbum = {
      id: 123, name: 'Batman'
    } as unknown as Album;

    const fakeResponse: Response = new Response(JSON.stringify({
      albums: {
        items: [
          mockAlbum
        ]
      } as unknown as AlbumSearchResponse['albums']
    } as AlbumSearchResponse), {
      status: 200
    })

    fetchSpy.mockResolvedValue(fakeResponse)

    // search.searchAlbums('batman').then(resp => {
    //   expect(resp).toEqual([mockAlbum])
    // })
    expect(search.searchAlbums('batman')).resolves.toEqual([mockAlbum])

    expect(fetchSpy).toHaveBeenCalledWith(
      `https://api.spotify.com/v1/search?type=album&query=batman`, {
      headers: {
        Authorization: 'Bearer FAKE_TOKEN'
      }
    }
    )
  });

  it('handles server errors', () => {
    const { fetchSpy } = setUp()


    const fakeResponse: Response = new Response(JSON.stringify({
      "error": {
        "status": 401,
        "message": "Invalid access token"
      }
    }), {
      status: 401
    })

    fetchSpy.mockResolvedValue(fakeResponse)

    // search.searchAlbums('batman').then(resp => {
    //   // Not here
    // }).catch(err => {
    //   expect(err).toEqual(Error("Invalid access token"))
    // })

    expect(search.searchAlbums('batman')).rejects.toEqual(Error("Invalid access token"))

    expect(fetchSpy).toHaveBeenCalledWith(
      `https://api.spotify.com/v1/search?type=album&query=batman`, {
      headers: {
        Authorization: 'Bearer FAKE_TOKEN'
      }
    }
    )
  })

});