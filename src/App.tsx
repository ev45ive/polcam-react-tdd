import React, { FC } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Playlists from './views/Playlists';
import MusicSearch from './views/MusicSearch';

import { Route, NavLink, Switch, Redirect } from 'react-router-dom'

export const App: FC = () => {

  return (
    <div className="App">

      <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand" to="/">Music App</NavLink>

          <button className="navbar-toggler" type="button">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/search">Search</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">
            <Switch>
              <Redirect from='/' exact={true} to="/playlists" />
              <Route path="/playlists" component={Playlists} />
              <Route path="/search" component={MusicSearch} />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}
App.displayName = "MyApp"

export default App;
