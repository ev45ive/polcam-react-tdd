
// class Playlist implements Playlist {
//   name: string;

//   constructor(name: string) {
//     this.name = name
//   }
// }

export interface Entity {
  // id: number | string
  id: string
  name: string
}

export interface Track extends Entity {
  type: 'track'
  duration: number
}

export interface Playlist extends Entity {
  type: 'playlist'
  /**
   * Is playlist public or private
   * @see http://developer.spotify.com/
   */
  public: boolean
  description: string
  // tracks: Array<Track>
  tracks?: Track[]
}

// const p: Playlist = {
//   id: '123',
//   name: '123',
//   type: 'playlist',
//   public:false,
//   description: '',
//   // tracks: [ ]
// }

// type PorT = Playlist | Track

// const p: Playlist | Track = {}

// if(p.type == 'playlist'){
//   p.tracks
// }else{
//   p.duration
// }

// if(p.tracks){ p.tracks.length }
// p.tracks && p.tracks.length
// p.tracks?.length

// if (typeof p.id === 'string') {
//   p.id.bold()
// } else {
//   p.id.toExponential()
// }



// interface Point { x: number; y: number }
// interface Vector { x: number; y: number, length: number }

// let v: Vector = { x: 213, y: 425, length: 123 }
// let p: Point = { x: 213, y: 425 }

// p = v
// v = p
