
export default {
  auth_url: 'AuthorizeURL',
  client_id: 'CLIENT_ID',
  redirect_uri: 'APP_URL',
  response_type: 'token'
}