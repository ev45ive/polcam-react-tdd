import { Album } from "../model/Album";
import { mount } from "enzyme";
import SearchResults from "./SearchResults";
import React from "react";
import AlbumCard from "./AlbumCard";

describe('AlbumCard', () => {

  const setUp = () => {
    // const wrapper = shallow()
    const mockResult: Album = { id: '13', name: 'test', images: [] } as unknown as Album;
    const wrapper = mount(<AlbumCard album={mockResult} />);

    return { wrapper, mockResults: mockResult };
  };


  it('renders album data', () => {
    const { wrapper } = setUp()
    expect(wrapper).toMatchSnapshot()
  })
})
