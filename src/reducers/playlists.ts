
import { Reducer, Action, ActionCreator } from 'redux'
import { Playlist } from '../model/Playlist'

const initialState = {
  playlists: [],
  selected: null
}

export type PlaylistsState = {
  playlists: Playlist[];
  selected: null | Playlist['id'];
}

const reducer: Reducer<PlaylistsState, Actions> = (state = initialState, action) => {

  switch (action.type) {

    case 'PLAYLISTS_LOAD_SUCCESS':
      return { ...state, playlists: action.payload.data }

    case 'PLAYLISTS_SELECT':
      return { ...state, selected: action.payload.id }

    case 'PLAYLISTS_UPDATE': {
      const updated = action.payload.data
      return { ...state, playlists: state.playlists.map(p => p.id === updated.id ? updated : p) }
    }

    default:
      return state
  }
}

export default reducer

type Actions =
  | PLAYLISTS_LOAD_START
  | PLAYLISTS_LOAD_SUCCESS
  | PLAYLISTS_LOAD_FAILED
  | PLAYLISTS_SELECT
  | PLAYLISTS_UPDATE


export interface PLAYLISTS_LOAD_START extends Action<'PLAYLISTS_LOAD_START'> { }
export interface PLAYLISTS_LOAD_FAILED extends Action<'PLAYLISTS_LOAD_FAILED'> { error: Error }
export interface PLAYLISTS_LOAD_SUCCESS extends Action<'PLAYLISTS_LOAD_SUCCESS'> {
  payload: {
    data: Playlist[]
  }
}

export interface PLAYLISTS_SELECT extends Action<'PLAYLISTS_SELECT'> {
  payload: {
    id: Playlist['id']
  }
}

export interface PLAYLISTS_UPDATE extends Action<'PLAYLISTS_UPDATE'> {
  payload: {
    data: Playlist
  }
}

const loadSuccess = (data: Playlist[]): PLAYLISTS_LOAD_SUCCESS => ({
  type: 'PLAYLISTS_LOAD_SUCCESS',
  payload: {
    data
  }
})

const select = (id: Playlist['id']): PLAYLISTS_SELECT => ({
  type: 'PLAYLISTS_SELECT',
  payload: {
    id
  }
})

const update = (data: Playlist): PLAYLISTS_UPDATE => ({
  type: 'PLAYLISTS_UPDATE',
  payload: {
    data
  }
})

export const actions = {
  loadSuccess,
  select,
  update
}


