import React from 'react'
import { shallow, mount } from 'enzyme'
import MusicSearch from './MusicSearch'
import SearchForm from '../components/SearchForm'
import SearchResults from '../components/SearchResults'
import { searchAlbums } from '../services/search'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { act } from 'react-dom/test-utils';

// load fakes from  ../services/__mocks__/search
jest.mock('../services/search')
jest.mock('../components/SearchForm', () => ({
  __esModule: true, // this property makes it work
  default: () => <div>SearchForm</div>
}))
jest.mock('../components/SearchResults', () => ({
  __esModule: true, // this property makes it work
  default: () => <div>SearchResults</div>
}))

describe('MusicSearch View', () => {

  const setUp = () => {
    const history = createMemoryHistory()
    history.push('/search')

    let wrapper = shallow(<MusicSearch />)

    // <Router history={history}></Router>
    // wrapper.update()
    // const wrapper = shallow(<MusicSearch />)
    const getForm = () => wrapper.find(SearchForm)
    const getResults = () => wrapper.find(SearchResults)
    const searchAlbumMock = (searchAlbums as jest.Mock)

    return {
      wrapper, getForm, getResults, searchAlbumMock
    }
  }

  it('gets query from url', () => {

    const { wrapper, getForm } = setUp()
    expect(getForm().prop('query')).toBe('')
  })

  it('is created', () => {
    const { wrapper } = setUp()
    expect(wrapper).toExist()
  })

  it('displays search form', () => {
    const { wrapper, getForm } = setUp()
    expect(getForm()).toExist()
  })

  it('displays search results', () => {
    const { wrapper, getResults } = setUp()
    expect(getResults()).toExist()

  })

  it('fetch data from service when form onSearch', () => {
    // Arrange - Given ...
    const { wrapper, getForm } = setUp()
    const form = getForm()

    // Act - When ...
    const onSearch = form.prop('onSearch')
    onSearch('batman')

    // Assert - Then ...
    expect(searchAlbums).toHaveBeenCalledWith('batman')
    // console.log(searchAlbums.mock.calls[0])
  })

  // it.skip('update results with new data', (done) => {
  // it.only('update results with new data', (done) => {
  it('update results with new data', (done) => {
    // Arrange - Given ...
    const { wrapper, getForm, getResults, searchAlbumMock } = setUp()
    const form = getForm()

    // Assert - Then ...
    expect(getResults().prop('results')).toEqual([])

    searchAlbumMock.mockImplementation(() => new Promise(resolve => {

      resolve([1, 2, 3])

      process.nextTick(() => {
        expect(getResults().prop('results')).toEqual([1, 2, 3])
        done()
      })
    }))

    // Act - When ...
    form.invoke('onSearch')('batman');
  })
})




