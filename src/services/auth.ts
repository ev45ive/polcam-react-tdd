import authConfig from "./auth.config";

let token: string | null = null;

export const getToken = () => {
  return token;
};


export const authorize = () => {
  const { auth_url, client_id, redirect_uri, response_type } = authConfig
  const params = new URLSearchParams({
    client_id, redirect_uri, response_type
  })
  window.location.href = (`${auth_url}?${params.toString()}`)
};


export const extractToken = () => {
  const params = new URLSearchParams(window.location.hash)
  const hash_token = params.get('#access_token')

  if (hash_token) {
    token = hash_token
  }
  return token;
};

