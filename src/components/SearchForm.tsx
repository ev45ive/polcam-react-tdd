import React, { useState, useEffect, useRef } from 'react'

interface Props {
  query: string,
  onSearch(query: string): void,
}

const SearchForm = ({ onSearch, query: parentQuery }: Props) => {
  const [query, setQuery] = useState('')
  const isFirst = useRef(true)

  const search = () => {
    onSearch(query)
  }

  useEffect(() => {
    setQuery(parentQuery);

    // console.log(parentQuery,query)
  }, [parentQuery])


  useEffect(() => {
    if (isFirst.current) { isFirst.current = false; return; }

    const handler = setTimeout(() => {
      onSearch(query)
    }, 300)

    return () => {
      clearTimeout(handler)
    }
  }, [query])


  return (
    <div className="input-group mb-3">
      <input type="text" className="form-control" placeholder="Search" name="query" value={query} autoFocus={true}
        onChange={e => setQuery(e.target.value)} />

      <div className="input-group-append">
        <button className="btn btn-outline-secondary" data-test="search-button" onClick={search}>Search</button>
      </div>
    </div>
  )
}

export default SearchForm
