
// tsrcc
import React, { PureComponent } from 'react'
import { Playlist } from '../model/Playlist'

interface Props {
  playlist: Playlist
  onCancel: () => void
  onSave: (playlist: Playlist) => void
}
interface State {
  playlist: Playlist,
}

export default class PlaylistForm extends PureComponent<Props, State> {

  state: State = {
    playlist: this.props.playlist,
  }

  constructor(props: Props) {
    super(props)
    // console.log('constructor')
  }

  componentDidMount() {
    // console.log('componentDidMount')
    this.nameInputRef.current!.focus()
  }

  static getDerivedStateFromProps(nextProps: Readonly<Props>, prevState: State): Partial<State> | null {
    // console.log('getDerivedStateFromProps')

    return {
      playlist: nextProps.playlist.id === prevState.playlist.id ? prevState.playlist : nextProps.playlist
    }
  }

  // shouldComponentUpdate(nextProps: Readonly<Props>, nextState: Readonly<State>, nextContext: any) {
  //    console.log('shouldComponentUpdate')
  //   return this.props.playlist !== nextProps.playlist || this.state.playlist !== nextState.playlist
  // }

  getSnapshotBeforeUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>) {
    // console.log('getSnapshotBeforeUpdate'); return { selection: 123, scroll: 123, focus: false }
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    // console.log('componentDidUpdate', snapshot)
  }

  componentWillUnmount() {
    // console.log('componentWillUnmount')
  }


  handleChange = (event: React.ChangeEvent<HTMLInputElement & HTMLTextAreaElement>) => {
    const type = event.target.type
    const value = type === 'checkbox' ? event.target.checked : event.target.value;
    const fieldName = event.target.name

    // ;(this.state.playlist as any)[fieldName] = value;

    // console.log('setState')
    this.setState((nextState) => ({
      playlist: {
        ...nextState.playlist,
        [fieldName]: value
      }
    }))
  }

  nameInputRef = React.createRef<HTMLInputElement>()

  render() {
    // console.log('render')
    return (
      <div>
        <pre>{JSON.stringify(this.props.playlist, null, 2)}</pre>
        <pre>{JSON.stringify(this.state.playlist, null, 2)}</pre>

        <div className="form-group">
          <label>Name:</label>
          <input type="text" className="form-control" name="name" ref={this.nameInputRef}
            value={this.state.playlist.name} onChange={this.handleChange} />
        </div>
        {170 - this.state.playlist.name.length} / 170

        <div className="form-group">
          <label>
            <input type="checkbox" name="public" checked={this.state.playlist.public} onChange={this.handleChange} /> Public</label>
        </div>

        <div className="form-group">
          <label>Description:</label>
          <textarea name="description" className="form-control" value={this.state.playlist.description} onChange={this.handleChange}></textarea>
        </div>

        <button className="btn-btn-danger"
          data-test="cancel-button" onClick={this.props.onCancel}>Cancel</button>

        <button className="btn-btn-success"
          data-test="save-button" onClick={
            () => this.props.onSave(this.state.playlist)
          }>Save</button>
      </div>
    )
  }
}
