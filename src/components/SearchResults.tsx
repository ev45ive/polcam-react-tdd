import React from 'react'
import { Album } from '../model/Album'
import AlbumCard from './AlbumCard'

interface Props {
  results: Album[]
}

const SearchResults = ({results}: Props) => {
  return (
    <div className="card-group">
      {
        results.map(result => {
          return <AlbumCard album={result} key={result.id}/>
        })
      }
    </div>
  )
}

export default SearchResults




