import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import { App } from './App';
// import { App as MyApp } from './App';
// import MyApp from './App';

import * as serviceWorker from './serviceWorker';
import { HashRouter as Router } from 'react-router-dom';
import { getToken, authorize, extractToken } from './services/auth';

// window.React = React;
// window.ReactDOM = ReactDOM;

extractToken()

if(!getToken()){
  authorize()
}

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
  </React.StrictMode>
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
