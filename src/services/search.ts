import { Album, AlbumSearchResponse } from "../model/Album"
import { getToken } from "./auth"

export const searchAlbums = (query: string): Promise<Album[]> => {
  return window.fetch(`https://api.spotify.com/v1/search?type=album&query=${query}`, {
    headers: {
      Authorization: 'Bearer ' + getToken()
    }
  })
  .then<AlbumSearchResponse>( resp => {
    if(resp.status >= 400){
      return resp.json().then(err => {
        return Promise.reject(Error(err.error.message))
      })
    }else{
      return resp.json()
    }
  })
  .then(resp => resp.albums.items)
}


