import { Album } from "../Album"

const albumData = [
  { id: '1', name: '1', images:[] },
  { id: '2', name: '2', images:[] },
  { id: '3', name: '3', images:[] },
] as unknown as Album[]

export default albumData