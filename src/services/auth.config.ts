
export default {
  auth_url: 'https://accounts.spotify.com/authorize',
  client_id: '7bc00fce3a3747c69381eb9f0b069c98',
  redirect_uri: 'http://localhost:3000/',
  response_type: 'token'
}