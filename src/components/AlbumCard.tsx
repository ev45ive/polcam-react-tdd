import React from 'react';
import { Album } from '../model/Album';
import './AlbumCard.css'

export interface Props {
  album: Album
}

export const AlbumCard = ({ album }: Props) => {
  return (
    <div className="card">
      {album.images.length && <img src={album.images[0].url} className="card-img-top" />}
      <div className="card-body">
        <h5 className="card-title">{album.name}</h5>
        {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
        {/* <a href="#" className="btn btn-primary">Go somewhere</a> */}
      </div>
    </div>
  );
};
export default AlbumCard;
